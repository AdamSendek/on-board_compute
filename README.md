# On-board_compute
Last update: 02-11-2018

## Description
On-board computer for electric vehicles of the MELEX type.
The computer shows the amount of energy in the batteries, the speed of the vehicle and the distance traveled.
The maximum battery voltage is 42V.


## License
Creative Commons Attribution-NonCommercial-ShareAlike 4.0
